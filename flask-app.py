import socket
import os
from dotenv import load_dotenv
from flask import Flask, render_template, escape, request

load_dotenv()

VERSION = os.getenv("FLASK_APP_VERSION")

app = Flask(__name__)

def return_hostname():
  return socket.gethostname()

def get_ip(hostname):
  return socket.gethostbyname(hostname)

# RUN with
# env FLASK_APP=flask-app.py flask run
@app.route('/')
def flaskapp():
  hostname = return_hostname()
  ip = get_ip(hostname)
  return render_template('index.html',hostname=hostname,ip=ip,version=VERSION)

@app.route('/path/<path:subpath>')
def show_subpath(subpath):
    # show the subpath after /path/
    return 'Subpath %s' % escape(subpath)

if __name__ == '__main__':
  app.run()
